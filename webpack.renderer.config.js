const rules = require('./webpack.rules')

rules.push({
  test: /\.css$/,
  use: ["style-loader", "css-loader", "postcss-loader"],
})

// Transpile code with babel
rules.push({
  test: /\.m?jsx?$/,
  exclude: /node_modules/,
  use: {
    loader: 'babel-loader',
    options: {
      presets: ['@babel/preset-env', '@babel/preset-react'],
      plugins: ['transform-jsx-classname-components', 'transform-react-pug'],
    }
  }
})

module.exports = {
  // Put your normal webpack config below here
  module: {
    rules,
  },
  resolve: {
    alias: {
      'react': 'preact/compat',
      'react-dom': 'preact/compat'
    },
  }
}
