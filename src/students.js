import React from 'react'
import fuzzysearch from 'fuzzysearch'
import {TabContent, Button, TextField} from './core'
import {useLdapConnection} from './useLdapConnection'
import {useLdapSearch} from './useLdapSearch'
import {AddStudentModal} from './addStudentModal'
import {EditStudentModal} from './editStudentModal'

export const Students = () => {
  //
  // Student Records
  //
  const {results, status, error, search} = useLdapSearch()
  const ldap = useLdapConnection()

  const loadStudents = () =>
    search({
      base: 'ou=students,ou=people,dc=techlitafrica,dc=org',
      scope: 'sub',
      filter: '(uidNumber=*)',
    })

  React.useEffect(() => {
    if (!ldap.connected) return
    loadStudents()
    const interval = setInterval(loadStudents, 10000)
    return () => clearInterval(interval)
  }, [ldap.connected])

  //
  // Add & Edit Student Modals
  //
  const formInit = {addingStudent: false, editingStudent: null}

  const [{addingStudent, editingStudent}, update] =
    React.useReducer((s1, s2) => ({...s1, ...s2}), formInit)

  const startAddingStudent = () =>
    update({addingStudent: true, editingStudent: null})

  const startEditingStudent = (editingStudent) =>
    update({addingStudent: false, editingStudent})

  const reset = () => {
    update(formInit)
    loadStudents()
  }

  //
  // Filter And Sort
  //
  const [filter, setFilter] = React.useState('')
  const [reverse, setReverse] = React.useState(false)

  const students = React.useMemo(() => {
    return results
      .filter((student) =>
        fuzzysearch(filter.toLowerCase(), student.displayName.toLowerCase()) ||
        fuzzysearch(filter.toLowerCase(), student.firstName.toLowerCase()) ||
        fuzzysearch(filter.toLowerCase(), student.lastName.toLowerCase()) ||
        fuzzysearch(filter.toLowerCase(), student.username.toLowerCase())
      )
      .sort((a, b) =>
        reverse
          ? a.username < b.username
          : a.username > b.username
      )
  }, [filter, reverse, results])

  return pug`
    TabContent
      .pb-4
        case status
          when 'failure'
            .p-4.border.border-red-400.text-red-300 Search failed: #{error}
          when 'connected'
            .p-4.border.border-green-400.text-green-300 Search worked

      .flex.flex-row.justify-between.pb-4
        .p-0
          TextField(
            value=filter
            onChange=(e)=>setFilter(e.target.value)
            placeholder='Filter'
          )
        .p-0
          Button(onClick=startAddingStudent) Add a Student

      .relative.max-w-96.border.border-gray-400
        if results.length < 1
          .text-lg.p-4.pb-24 This server doesn't have any students yet!

        else if students.length < 1
          .text-lg.p-4.pb-24 No students matched '#{filter}'

        else
          for student in students
            .flex.justify-between.p-2.hover_bg-gray-900.cursor-pointer.border-b.border-gray-600(key=student.id)
              .flex
                .h-16.w-16.bg-gray-500.rounded-full

                .pl-2.flex.flex-col
                  .text-lg= student.displayName

                  .flex.items-start.text-gray-400
                    .mt-1.p-1.text-xs.border.border-gray-400= student.id
                    .pl-2.text-xl.font-mono= student.username

              .p-0
                Button(onClick=()=>startEditingStudent(student)) Edit

        if status == 'searching'
          .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
            .text-2xl.text-white searching...

    if addingStudent
      AddStudentModal(close=reset)

    if editingStudent
      EditStudentModal(init=editingStudent close=reset)
  `
}
