import React from 'react'
import {Modal, Button, ipcSend, RedButton, TextField, useIpcListener} from "./core"

export const AddStudentModal = ({close}) => {
  const [{
    firstName,
    lastName,
    username,
    password,
    usernameTouched,
    status,
    error,
  }, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      firstName: '',
      lastName: '',
      username: '',
      password: '',
      usernameTouched: false,
      status: 'idle',
      error: null,
    }
  )

  const updateFirstName = (e) => {
    const firstName = e.target.value

    update({
      firstName,
      username: (
        usernameTouched
          ? username
          : firstName.replaceAll(' ', '')[0].toLowerCase() + lastName.replaceAll(' ', '').toLowerCase()
      ),
    })
  }

  const updateLastName = (e) => {
    const lastName = e.target.value

    update({
      lastName,
      username: (
        usernameTouched
          ? username
          : firstName.replaceAll(' ', '')[0].toLowerCase() + lastName.replaceAll(' ', '').toLowerCase()
      ),
    })
  }

  const updateUsername = (e) =>
    update({
      username: e.target.value,
      usernameTouched: true,
    })

  const updatePassword = (e) =>
    update({password: e.target.value})

  const save = () => {
    update({status: 'saving'})
    ipcSend('ldap:create-student', {firstName, lastName, username, password})
  }

  useIpcListener('ldap:create-student-failure', (_event, error) => {
    console.log(error)
    update({status: 'failure', error})
  })

  useIpcListener('ldap:create-student-success', () => {
    update({
      firstName: '',
      lastName: '',
      username: '',
      password: '',
      usernameTouched: false,
      status: 'success',
    })
  })
  
  return pug`
    Modal.w-96
      .flex.flex-row.items-center.justify-between.pb-4
        .text-lg Add a Student
        .p-0
          RedButton(onClick=close) close

      .pb-4
        case status
          when 'failure'
            .p-4.border.border-red-400.text-red-300 Failed to save student: #{error}
          when 'success'
            .p-4.border.border-green-400.text-green-300 Student saved
      
      label First Name
      .pl-2.pb-2
        TextField(
          disabled=(status == 'saving')
          value=firstName
          onChange=updateFirstName
          placeholder='First Name'
        )
      
      label Last Name
      .pl-2.pb-2
        TextField(
          disabled=(status == 'saving')
          value=lastName
          onChange=updateLastName
          placeholder='Last Name'
        )

      label Username
      .pl-2.pb-2
        TextField(
          disabled=(status == 'saving')
          value=username
          onChange=updateUsername
          placeholder='Username'
        )

      label Password
      .pl-2.pb-2
        TextField(
          disabled=(status == 'saving')
          value=password
          onChange=updatePassword
          placeholder='Password'
        )

      .pt-4.text-right
        Button(
          disabled=(status == 'saving')
          onClick=save
        ) Save New Student

    
      if status == 'saving'
        .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
          .text-2xl.text-white saving...
  `
}