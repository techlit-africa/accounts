import path from 'path'
import fs from 'fs'
import fetch from 'node-fetch'

import { ipcMain } from 'electron'

import { Client, Change, Attribute } from 'ldapts'
import ssha from 'ssha'

import { ipcSend } from './mainWindow'

var client

//
// Helpers
//
const getNewUid = async () => {
  const { searchEntries } = await client.search('ou=people,dc=techlitafrica,dc=org', {
    filter: '(uidNumber=*)',
    scope: 'sub',
    attributes: ['uidNumber'],
  })

  var lastUid = 1001
  searchEntries.forEach(({uidNumber}) => {
    const uid = parseInt(uidNumber)
    if (uid > lastUid) lastUid = uid
  })
  return lastUid + 1
}

const getUidForUsername = async (username) => {
  const { searchEntries } = await client.search('ou=people,dc=techlitafrica,dc=org', {
    filter: `(uid=${username})`,
    scope: 'sub',
    attributes: ['uidNumber'],
  })

  return searchEntries?.[0]?.uidNumber
}

export const initLdapClient = () => {
  ipcMain.on('config:get', async (_event, data) => {
    try {
      const rep = await fetch('http://localhost:5555/config')
      const config = await rep.json()
      ipcSend('config:get-success', config)
    } catch (err) {
      console.error(err)
      ipcSend('config:get-failure', err.message)
    }
  })

  //
  // Core
  //
  ipcMain.on('ldap:connect', async (_event, data) => {
    try {
      client = new Client({
        url: data.uri,
        tlsOptions: {
          rejectUnauthorized: false,
        }
      })

      await client.bind(data.dn, data.password)
      ipcSend('ldap:connect-success', { ping: 'pong' })

    } catch (err) {
      console.error(err)
      ipcSend('ldap:connect-failure', err.message)
    }
  })

  ipcMain.on('ldap:disconnect', async () => {
    try {
      await client.unbind()
    } catch (err) {
      console.error(err)
    }
  })

  //
  // Search for People
  //
  ipcMain.on('ldap:search', async (_event, { base, ...opts }) => {
    try {
      const { searchEntries } = await client.search(base, opts)

      const people = searchEntries.map((entry) => ({
        id: entry.uidNumber,
        home: entry.homeDirectory,
        shell: entry.loginShell,
        username: entry.uid,
        firstName: entry.givenName,
        lastName: entry.sn,
        displayName: entry.cn,
      }))
      ipcSend('ldap:search-success', people)

    } catch (err) {
      console.error(err)
      ipcSend('ldap:search-failure', err.message)
    }
  })

  //
  // Students
  //
  ipcMain.on('ldap:create-student', async (_event, { firstName, lastName, username, password }) => {
    try {
      const id = await getNewUid()
      const conflictingUid = await getUidForUsername(username)
      if (conflictingUid) {
        console.log('Not creating student: username is taken')
        ipcSend('ldap:create-student-failure', 'username is taken')
        return
      }

      await client.add(`uidNumber=${id},ou=students,ou=people,dc=techlitafrica,dc=org`, {
        objectClass: [
          'top',
          'organizationalPerson',
          'inetOrgPerson',
          'posixAccount',
          'shadowAccount',
        ],
        cn: `${firstName} ${lastName}`,
        givenName: firstName,
        sn: lastName,
        uid: username,
        uidNumber: `${id}`,
        gidNumber: process.env.STUDENTS_GID || '1003',
        userPassword: ssha.create(password),
        loginShell: '/bin/bash',
        homeDirectory: `/home/${username}`
      })
      ipcSend('ldap:create-student-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:create-student-failure', err.message)
    }
  })

  ipcMain.on('ldap:update-student', async (_event, { id, displayName, firstName, lastName, username }) => {
    try {
      const conflictingUid = await getUidForUsername(username)
      if (conflictingUid && conflictingUid != id) {
        console.log('Not updating student: username is taken')
        ipcSend('ldap:update-student-failure', 'username is taken')
        return
      }

      const name = `uidNumber=${id},ou=students,ou=people,dc=techlitafrica,dc=org`

      const changes = [
        ['cn',        displayName],
        ['givenName', firstName],
        ['sn',        lastName],
        ['uid',       username],
      ].map(([type, value]) =>
        new Change({
          operation: 'replace',
          modification: new Attribute({type, values: [value]}),
        }))

      await client.modify(name, changes)
      ipcSend('ldap:update-student-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:update-student-failure', err.message)
    }
  })

  ipcMain.on('ldap:reset-student-password', async (_event, { id, password }) => {
    try {
      const name = `uidNumber=${id},ou=students,ou=people,dc=techlitafrica,dc=org`
      const change = new Change({
        operation: 'replace',
        modification: new Attribute({
          type: 'userPassword',
          values: [ssha.create(password)],
        }),
      })
      await client.modify(name, change)
      ipcSend('ldap:update-student-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:update-student-failure', err.message)
    }
  })

  ipcMain.on('ldap:delete-student', async (_event, { id }) => {
    try {
      await client.del(`uidNumber=${id},ou=students,ou=people,dc=techlitafrica,dc=org`)
      ipcSend('ldap:delete-student-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:delete-student-failure', err.message)
    }
  })

  //
  // Teachers
  //
  ipcMain.on('ldap:create-teacher', async (_event, { firstName, lastName, username, password }) => {
    try {
      const id = await getNewUid()
      const conflictingUid = await getUidForUsername(username)
      if (conflictingUid) {
        console.log('Not creating teacher: username is taken')
        ipcSend('ldap:create-teacher-failure', 'username is taken')
        return
      }

      await client.add(`uidNumber=${id},ou=teachers,ou=people,dc=techlitafrica,dc=org`, {
        objectClass: [
          'top',
          'organizationalPerson',
          'inetOrgPerson',
          'posixAccount',
          'shadowAccount',
        ],
        cn: `${firstName} ${lastName}`,
        givenName: firstName,
        sn: lastName,
        uid: username,
        uidNumber: `${id}`,
        gidNumber: process.env.TEACHERS_GID || '1004',
        userPassword: ssha.create(password),
        loginShell: '/bin/bash',
        homeDirectory: `/home/${username}`
      })
      ipcSend('ldap:create-teacher-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:create-teacher-failure', err.message)
    }
  })

  ipcMain.on('ldap:update-teacher', async (_event, { id, displayName, firstName, lastName, username }) => {
    try {
      const conflictingUid = await getUidForUsername(username)
      if (conflictingUid && conflictingUid != id) {
        console.log('Not updating teacher: username is taken')
        ipcSend('ldap:update-teacher-failure', 'username is taken')
        return
      }

      const name = `uidNumber=${id},ou=teachers,ou=people,dc=techlitafrica,dc=org`

      const changes = [
        ['cn',        displayName],
        ['givenName', firstName],
        ['sn',        lastName],
        ['uid',       username],
      ].map(([type, value]) =>
        new Change({
          operation: 'replace',
          modification: new Attribute({type, values: [value]}),
        }))

      await client.modify(name, changes)
      ipcSend('ldap:update-teacher-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:update-teacher-failure', err.message)
    }
  })

  ipcMain.on('ldap:reset-teacher-password', async (_event, { id, password }) => {
    try {
      const name = `uidNumber=${id},ou=teachers,ou=people,dc=techlitafrica,dc=org`
      const change = new Change({
        operation: 'replace',
        modification: new Attribute({
          type: 'userPassword',
          values: [ssha.create(password)],
        }),
      })
      await client.modify(name, change)
      ipcSend('ldap:update-teacher-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:update-teacher-failure', err.message)
    }
  })

  ipcMain.on('ldap:delete-teacher', async (_event, { id }) => {
    try {
      await client.del(`uidNumber=${id},ou=teachers,ou=people,dc=techlitafrica,dc=org`)
      ipcSend('ldap:delete-teacher-success')

    } catch (err) {
      console.error(err)
      ipcSend('ldap:delete-teacher-failure', err.message)
    }
  })
}
