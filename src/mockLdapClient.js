import {ipcMain} from 'electron'

import {ipcSend} from './mainWindow'

export const initLdapClient = () => {
  ipcMain.on('ldap:connect', () => {
    if (Math.random() < 0.3) return ipcSend('ldap:connect-failure', {})

    ipcSend('ldap:connect-success', { ping: 'pong' })
  })

  ipcMain.on('ldap:disconnect', () => {})

  ipcMain.on('ldap:search', () => {
    if (Math.random() < 0.3) return ipcSend('ldap:search-failure', {})

    ipcSend('ldap:search-success', ['asdf', 'qwer', 'zxcv', 'poiu', 'lkjh', 'mnbv'].map((name, i) => ({
      //
      // TODO: SCHEMA CHANGED
      //
      
      // objectName: `${name.toLowerCase()},ou=students,ou=people,dc=techlitafrica,dc=org`,
      // attributes: [
      //   {type: 'objectClass', _vals: ['top', 'organizationalPerson', 'inetOrgPerson', 'posixAccount', 'shadowAccount']},
      //   {type: 'cn', _vals: [`${name} ${name}`]},
      //   {type: 'sn', _vals: [name]},
      //   {type: 'givenName', _vals: [name]},
      //   {type: 'uid', _vals: [name.toLowerCase()]},
      //   {type: 'uidNumber', _vals: [`100${i+2}`]},
      //   {type: 'gidNumber', _vals: [`100${i+2}`]},
      //   {type: 'userPassword', _vals: ['{SSHA}H45H3DPA55W0rD']},
      //   {type: 'loginShell', _vals: ['/bin/bash']},
      //   {type: 'homeDirectory', _vals: [`/home/${name.toLowerCase()}`]},
      // ],
    })))
  })

  ipcMain.on('ldap:create-student', (_event, { firstName, lastName, username, password }) => {
    //
    // TODO: UNFINISHED
    //
    // client?.search('dc=techlitafrica,dc=org', {
    //   filter: 'uidNumber=*',
    //   scope: 'sub',
    //   attributes: ['uidNumber'],
    // }, (err, search) => {
    //   if (err) {
    //     ipcSend('ldap:create-student-failure', err)
    //     return
    //   }

    //   var lastUid = 1001
    //   search.on('searchEntry', (entry) => {
    //     const uid = parseInt(entry.attributes[0]._vals)
    //     if (uid > lastUid) lastUid = uid
    //   })

    //   search.on('error', (err) => {
    //     ipcSend('ldap:create-student-failure', err)
    //   })

    //   search.on('end', (result) => {
    //     client?.add(`uid=${username},ou=students,ou=people,dc=techlitafrica,dc=org`, {
    //       objectClass: [
    //         'top',
    //         'organizationalPerson',
    //         'inetOrgPerson',
    //         'posixAccount',
    //         'shadowAccount',
    //       ],
    //       cn: `${firstName} ${lastName}`,
    //       givenName: firstName,
    //       sn: lastName,
    //       uid: username,
    //       uidNumber: lastUid + 1,
    //       gidNumber: process.env.STUDENTS_GID,
    //       userPassword: ssha.create(password),
    //       loginShell: '/bin/bash',
    //       homeDirectory: `/home/${username}`
    //     }, (err) => {
    //       if (err) {
    //         ipcSend('ldap:create-student-failure', err)
    //         return
    //       }
    //       ipcSend('ldap:create-student-success')
    //     })
    //   })
    // })
  })
}
