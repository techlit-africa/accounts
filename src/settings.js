import React from 'react'
import {TextField, Button, TabContent} from './core'
import {useLdapConnection} from './useLdapConnection'

export const Settings = () => {
  const ldap = useLdapConnection()
  
  return pug`
    TabContent
      .pb-4
        .text-xl.font-medium LDAP Server Connection 

      .relative.max-w-96.p-4.border.border-gray-400
        label URI
        .pl-2.pb-2
          TextField(
            placeholder='URI'
            value=ldap.uri
            onChange=(e)=>ldap.update({uri: e.target.value})
            disabled=(ldap.status == 'connecting')
          )

        label DN
        .pl-2.pb-2
          TextField(
            placeholder='DN'
            value=ldap.dn
            onChange=(e)=>ldap.update({dn: e.target.value})
            disabled=(ldap.status == 'connecting')
          )

        label Password
        .pl-2.pb-2
          TextField(
            placeholder='Password'
            value=ldap.password
            onChange=(e)=>ldap.update({password: e.target.value})
            disabled=(ldap.status == 'connecting')
          )

        .pt-4.text-right
          Button(
            disabled=(ldap.status == 'connecting')
            onClick=ldap.connect
          ) Connect to LDAP Server

        if ldap.status == 'connecting'
          .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
            .text-2xl.text-white connecting...

      .pt-4
        case ldap.status
          when 'failure'
            .p-4.border.border-red-400.text-red-300 Connection failed
          when 'connected'
            .p-4.border.border-green-400.text-green-300 Connected successfully
  `
}