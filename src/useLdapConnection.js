import React from 'react'
import {ipcSend, useIpcListener} from './core'

export const LdapConnectionContext = React.createContext({})

export const useLdapConnection = () => React.useContext(LdapConnectionContext)

export const LdapConnectionProvider = ({children}) => {
  const [{uri, dn, password, status, error}, update] =
    React.useReducer(
      (s1, s2) => ({...s1, ...s2}),
      {
        uri: null,
        dn: 'cn=admin,dc=techlitafrica,dc=org',
        password: 'empowermogotio',
        status: 'disconnected',
        error: null,
      }
    )

  const connected = status == 'connected'

  const connect = () => {
    update({status: 'connecting'})
    ipcSend('ldap:connect', {uri, dn, password})
  }

  useIpcListener('ldap:connect-failure', (_event, error) => {
    update({status: 'failure', error})
  }, [])

  useIpcListener('ldap:connect-success', (_event) => {
    update({status: 'connected'})
  }, [])

  const getServerIp = () => {
    update({status: 'connecting'})
    ipcSend('config:get')
  }

  useIpcListener('config:get-failure', (_event, error) => {
    update({status: 'failure', error})
  }, [])

  useIpcListener('config:get-success', (_event, config) => {
    update({uri: `ldaps://${config.server_ip}`})
  }, [])

  React.useEffect(() => {
    getServerIp()
  }, [])

  React.useEffect(() => {
    if (uri && !connected) connect()
  }, [uri, connected])

  const value = {uri, dn, password, status, error, connected, update, connect}

  return pug`
    LdapConnectionContext.Provider(value=value)= children
  `
}
