import React from 'react'

export const ipcSend = (channel, data) =>
  window.ipc.send(channel, data)

export const useIpcListener = (channel, listener) => {
  React.useEffect(() => {
    window.ipc.on(channel, listener)
    return () => window.ipc.removeListener(channel, listener) 
  }, [])
}

export const Layout = ({children}) => pug`
  .flex.flex-col.w-screen.h-screen.bg-gray-800= children
`

export const Modal = ({className, children}) => pug`
.fixed.inset-0.flex.items-center.justify-center.overflow-y-scroll.bg-black.bg-opacity-50
  .relative.p-4.text-gray-200.bg-gray-800.border.border-gray-400(className=className)
    = children
`

export const Tabs = ({children}) => pug`
  .flex.flex-row.h-10.bg-gray-600.text-white= children
`

export const Tab = ({children, active, ...props}) => {
  const border = active ? '' : 'border-2 '
  const colors = active
    ? 'bg-gray-800 text-white '
    : 'bg-gray-600 text-gray-200 border-gray-200 '

  return pug`
    button.flex-1.h-10(
      className=(border + colors)
      ...props
    )
      = children
  `
}

export const TabContent = ({children}) => pug`
  .flex-1.overflow-y-scroll.p-4.text-gray-200= children
`

export const TextField = ({...props}) => {
  const text = 'text-gray-200 hover_text-gray-100 focus_text-white '
  const background = 'bg-gray-600 hover_bg-gray-700 focus_bg-gray-800 '
  const disabledStyles = props.disabled ? 'bg-gray-700 text-gray-200 ' : ''

  return pug`
    input.w-full.p-2.transition(
      className=(text + background + disabledStyles)
      ...props
    )
  `
}

export const Button = ({children, ...props}) => {
  const border = 'border-gray-400 hover_border-200 active-border-white '
  const background = 'bg-gray-700 hover_bg-gray-900 active_bg-black '
  
  return pug`
    button.w-full.text-white.p-2.border-2.transition(
      className=(border + background)
      ...props
    )
      = children
  `
}

export const RedButton = ({children, ...props}) => {
  const border = 'border-red-400 hover_border-200 active-border-white '
  const background = 'bg-gray-800 hover_bg-gray-900 active_bg-black '
  
  return pug`
    button.w-full.text-red-500.p-2.border.transition(
      className=(border + background)
      ...props
    )
      = children
  `
}