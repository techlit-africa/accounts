import React from 'react'
import { ipcSend, useIpcListener } from './core'

export const useLdapSearch = () => {
  const [{ results, status, error }, update] =
    React.useReducer(
      (s1, s2) => ({ ...s1, ...s2 }),
      {
        results: [],
        status: 'idle',
        error: null,
      }
    )

  const search = (opts) => {
    update({ status: 'searching' })
    ipcSend('ldap:search', opts)
  }

  useIpcListener('ldap:search-failure', (_event, error) => {
    update({ status: 'failure', error })
  }, [])

  useIpcListener('ldap:search-success', (_event, results) => {
    update({ status: 'connected', results })
  }, [])

  return {results, status, error, search}
}