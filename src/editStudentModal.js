import React from 'react'
import {Modal, Button, ipcSend, RedButton, TextField, useIpcListener} from "./core"

export const EditStudentModal = ({init, close}) => {
  const [{
    username,
    firstName,
    lastName,
    displayName,
    deletingStudent,
    resettingPassword,
    password,
    status,
    error,
  }, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      ...init,
      deletingStudent: false,
      resettingPassword: false,
      password: '',
      status: 'idle',
      error: null,
    }
  )

  const save = () => {
    update({status: 'saving'})
    ipcSend('ldap:update-student', {id: init.id, username, firstName, lastName, displayName})
  }

  const resetPassword = () => {
    update({status: 'saving'})
    ipcSend('ldap:reset-student-password', {id: init.id, password})
  }

  useIpcListener('ldap:update-student-failure', (_event, error) => {
    console.log(error)
    update({status: 'failure', error, password: ''})
  })

  useIpcListener('ldap:update-student-success', () => {
    update({status: 'success', resettingPassword: false, password: ''})
  })

  const deleteAndClose = () => {
    update({
      deletingStudent: false,
      status: 'saving',
    })
    ipcSend('ldap:delete-student', {id: init.id})
  }

  useIpcListener('ldap:delete-student-failure', (_event, error) => {
    console.log(error)
    update({status: 'failure', error})
  })

  useIpcListener('ldap:delete-student-success', () => {
    update({status: 'success'})
    close()
  })
  
  return pug`
    Modal.w-prose
      .flex.items-center.justify-between.pb-4
        .flex.flex-col
          .text-lg Edit Student
          a.text-sm.text-gray-400.hover_text-red-500.active_text-red-600.cursor-pointer(
            disabled=(status == 'saving')
            onClick=()=>update({deletingStudent: true})
          ) (Delete this Student)
        .p-0
          RedButton(onClick=close) close

      if deletingStudent
        Modal.w-96
          .flex.items-center.justify-between.pb-4
            .text-lg Deleting Student
            .p-0
              RedButton(onClick=()=>update({deletingStudent: false})) close

          .p-0 Are you sure?
          .flex.flex-row
            .flex-1.p-2
              Button(onClick=deleteAndClose) Yes
            .flex-1.p-2
              Button(onClick=()=>update({deletingStudent: false})) No

      if resettingPassword
        Modal.w-96
          .flex.items-center.justify-between.pb-4
            .text-lg Reset Student Password
            .p-0
              RedButton(onClick=()=>update({resettingPassword: false})) close

          .flex.flex-col
            label New Password
            .pl-2.pb-2
              TextField(
                disabled=(status == 'saving')
                value=password
                onChange=(e)=>update({password: e.target.value})
                placeholder='New Password'
              )

            .pt-4.text-right
              Button(
                disabled=(status == 'saving')
                onClick=resetPassword
              ) Reset Student Password
          
              
            if status == 'saving'
              .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
                .text-2xl.text-white saving...

      .pb-4
        case status
          when 'failure'
            .p-4.border.border-red-400.text-red-300 Failed to save student: #{error}
          when 'success'
            .p-4.border.border-green-400.text-green-300 Student saved
      
      .flex
        .flex.flex-col
          label Display Name
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=displayName
              onChange=(e)=>update({displayName: e.target.value})
              placeholder='Display Name'
            )

          label First Name
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=firstName
              onChange=(e)=>update({firstName: e.target.value})
              placeholder='First Name'
            )
          
          label Last Name
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=lastName
              onChange=(e)=>update({lastName: e.target.value})
              placeholder='Last Name'
            )

        .flex.flex-col.pl-4
          label Username
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=username
              onChange=(e)=>update({username: e.target.value})
              placeholder='username'
            )

          label Home
          .pl-2.pb-2
            TextField(disabled value=init.home)

          label Shell
          .pl-2.pb-2
            TextField(disabled value=init.shell)

          label UNIX ID
          .pl-2.pb-2
            TextField(disabled value=init.id)

      .pt-4.text-right
        Button(
          disabled=(status == 'saving')
          onClick=save
        ) Save Changes to Student

      .pt-4
        a.text-sm.text-gray-400.hover_text-yellow-500.active_text-yellow-600.cursor-pointer(
          disabled=(status == 'saving')
          onClick=()=>update({resettingPassword: true})
        ) (Reset Password)
  
      if status == 'saving'
        .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
          .text-2xl.text-white saving...
  `
}