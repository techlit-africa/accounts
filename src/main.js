import {initMainWindow} from './mainWindow'
import {initLdapClient} from './ldapClient'
import {initMockLdapClient} from './mockLdapClient'

initMainWindow()
process.env.MOCK_LDAP ? initMockLdapClient() : initLdapClient()