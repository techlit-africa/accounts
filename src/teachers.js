import React from 'react'
import fuzzysearch from 'fuzzysearch'
import {TabContent, Button, TextField} from './core'
import {useLdapConnection} from './useLdapConnection'
import {useLdapSearch} from './useLdapSearch'
import {AddTeacherModal} from './addTeacherModal'
import {EditTeacherModal} from './editTeacherModal'

export const Teachers = () => {
  //
  // Teacher Records
  //
  const {results, status, search} = useLdapSearch()
  const ldap = useLdapConnection()

  const loadTeachers = () =>
    search({
      base: 'ou=teachers,ou=people,dc=techlitafrica,dc=org',
      scope: 'sub',
      filter: '(uidNumber=*)',
    })

  React.useEffect(() => {
    if (!ldap.connected) return
    loadTeachers()
    const interval = setInterval(loadTeachers, 10000)
    return () => clearInterval(interval)
  }, [ldap.connected])

  //
  // Add & Edit Teacher Modals
  //
  const formInit = {addingTeacher: false, editingTeacher: null}

  const [{addingTeacher, editingTeacher}, update] =
    React.useReducer((s1, s2) => ({...s1, ...s2}), formInit)

  const startAddingTeacher = () =>
    update({addingTeacher: true, editingTeacher: null})

  const startEditingTeacher = (editingTeacher) =>
    update({addingTeacher: false, editingTeacher})

  const reset = () => {
    update(formInit)
    loadTeachers()
  }

  //
  // Filter And Sort
  //
  const [filter, setFilter] = React.useState('')
  const [reverse, setReverse] = React.useState(false)

  const teachers = React.useMemo(() => {
    return results
      .filter((teacher) =>
        fuzzysearch(filter.toLowerCase(), teacher.displayName.toLowerCase()) ||
        fuzzysearch(filter.toLowerCase(), teacher.firstName.toLowerCase()) ||
        fuzzysearch(filter.toLowerCase(), teacher.lastName.toLowerCase()) ||
        fuzzysearch(filter.toLowerCase(), teacher.username.toLowerCase())
      )
      .sort((a, b) =>
        reverse
          ? a.username < b.username
          : a.username > b.username
      )
  }, [filter, reverse, results])

  return pug`
    TabContent
      .pb-4
        case status
          when 'failure'
            .p-4.border.border-red-400.text-red-300 Search failed
          when 'connected'
            .p-4.border.border-green-400.text-green-300 Search worked

      .flex.flex-row.justify-between.pb-4
        .p-0
          TextField(
            value=filter
            onChange=(e)=>setFilter(e.target.value)
            placeholder='Filter'
          )
        .p-0
          Button(onClick=startAddingTeacher) Add a Teacher

      .relative.max-w-96.border.border-gray-400
        if results.length < 1
          .text-lg.p-4.pb-24 This server doesn't have any teachers yet!

        else if teachers.length < 1
          .text-lg.p-4.pb-24 No teachers matched '#{filter}'

        else
          for teacher in teachers
            .flex.justify-between.p-2.hover_bg-gray-900.cursor-pointer.border-b.border-gray-600(key=teacher.id)
              .flex
                .h-16.w-16.bg-gray-500.rounded-full

                .pl-2.flex.flex-col
                  .text-lg= teacher.displayName

                  .flex.items-start.text-gray-400
                    .mt-1.p-1.text-xs.border.border-gray-400= teacher.id
                    .pl-2.text-xl.font-mono= teacher.username

              .p-0
                Button(onClick=()=>startEditingTeacher(teacher)) Edit

        if status == 'searching'
          .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
            .text-2xl.text-white searching...

    if addingTeacher
      AddTeacherModal(close=reset)

    if editingTeacher
      EditTeacherModal(init=editingTeacher close=reset)
  `
}
