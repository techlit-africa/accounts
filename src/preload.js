import {contextBridge, ipcRenderer} from 'electron'

contextBridge.exposeInMainWorld('ipc', {
  send: ipcRenderer.send.bind(ipcRenderer),
  on: ipcRenderer.on.bind(ipcRenderer),
  removeListener: ipcRenderer.removeListener.bind(ipcRenderer),
})