import React from 'react'
import {Modal, Button, ipcSend, RedButton, TextField, useIpcListener} from "./core"

export const EditTeacherModal = ({init, close}) => {
  const [{
    username,
    firstName,
    lastName,
    displayName,
    deletingTeacher,
    resettingPassword,
    password,
    status,
    error,
  }, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      ...init,
      deletingTeacher: false,
      resettingPassword: false,
      password: '',
      status: 'idle',
      error: null,
    }
  )

  const save = () => {
    update({status: 'saving'})
    ipcSend('ldap:update-teacher', {id: init.id, username, firstName, lastName, displayName})
  }

  const resetPassword = () => {
    update({status: 'saving'})
    ipcSend('ldap:reset-teacher-password', {id: init.id, password})
  }

  useIpcListener('ldap:update-teacher-failure', (_event, error) => {
    console.log(error)
    update({status: 'failure', error, password: ''})
  })

  useIpcListener('ldap:update-teacher-success', () => {
    update({status: 'success', resettingPassword: false, password: ''})
  })

  const deleteAndClose = () => {
    update({
      deletingTeacher: false,
      status: 'saving',
    })
    ipcSend('ldap:delete-teacher', {id: init.id})
  }

  useIpcListener('ldap:delete-teacher-failure', (_event, error) => {
    console.log(error)
    update({status: 'failure', error})
  })

  useIpcListener('ldap:delete-teacher-success', () => {
    update({status: 'success'})
    close()
  })
  
  return pug`
    Modal.w-prose
      .flex.items-center.justify-between.pb-4
        .flex.flex-col
          .text-lg Edit Teacher
          a.text-sm.text-gray-400.hover_text-red-500.active_text-red-600.cursor-pointer(
            disabled=(status == 'saving')
            onClick=()=>update({deletingTeacher: true})
          ) (Delete this Teacher)
        .p-0
          RedButton(onClick=close) close

      if deletingTeacher
        Modal.w-96
          .flex.items-center.justify-between.pb-4
            .text-lg Deleting Teacher
            .p-0
              RedButton(onClick=()=>update({deletingTeacher: false})) close

          .p-0 Are you sure?
          .flex.flex-row
            .flex-1.p-2
              Button(onClick=deleteAndClose) Yes
            .flex-1.p-2
              Button(onClick=()=>update({deletingTeacher: false})) No

      if resettingPassword
        Modal.w-96
          .flex.items-center.justify-between.pb-4
            .text-lg Reset Teacher Password
            .p-0
              RedButton(onClick=()=>update({resettingPassword: false})) close

          .flex.flex-col
            label New Password
            .pl-2.pb-2
              TextField(
                disabled=(status == 'saving')
                value=password
                onChange=(e)=>update({password: e.target.value})
                placeholder='New Password'
              )

            .pt-4.text-right
              Button(
                disabled=(status == 'saving')
                onClick=resetPassword
              ) Reset Teacher Password
          
              
            if status == 'saving'
              .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
                .text-2xl.text-white saving...

      .pb-4
        case status
          when 'failure'
            .p-4.border.border-red-400.text-red-300 Failed to save teacher: #{error}
          when 'success'
            .p-4.border.border-green-400.text-green-300 Teacher saved
      
      .flex
        .flex.flex-col
          label Display Name
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=displayName
              onChange=(e)=>update({displayName: e.target.value})
              placeholder='Display Name'
            )

          label First Name
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=firstName
              onChange=(e)=>update({firstName: e.target.value})
              placeholder='First Name'
            )
          
          label Last Name
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=lastName
              onChange=(e)=>update({lastName: e.target.value})
              placeholder='Last Name'
            )

        .flex.flex-col.pl-4
          label Username
          .pl-2.pb-2
            TextField(
              disabled=(status == 'saving')
              value=username
              onChange=(e)=>update({username: e.target.value})
              placeholder='username'
            )

          label Home
          .pl-2.pb-2
            TextField(disabled value=init.home)

          label Shell
          .pl-2.pb-2
            TextField(disabled value=init.shell)

          label UNIX ID
          .pl-2.pb-2
            TextField(disabled value=init.id)

      .pt-4.text-right
        Button(
          disabled=(status == 'saving')
          onClick=save
        ) Save Changes to Teacher

      .pt-4
        a.text-sm.text-gray-400.hover_text-yellow-500.active_text-yellow-600.cursor-pointer(
          disabled=(status == 'saving')
          onClick=()=>update({resettingPassword: true})
        ) (Reset Password)

      if status == 'saving'
        .absolute.inset-0.flex.items-center.justify-center.bg-black.bg-opacity-50
          .text-2xl.text-white saving...
  `
}