import React from 'react'
import {render} from 'react-dom'
import {Layout, Tabs, Tab, TabContent} from './core'
import {LdapConnectionProvider, useLdapConnection} from './useLdapConnection'
import {Settings} from './settings'
import {Students} from './students'
import {Teachers} from './teachers'

import './main.css'

const Main = () => {
  const [tab, setTab] = React.useState('students')
  const ldap = useLdapConnection()

  return pug`
    Layout
      Tabs
        Tab(
          active=(tab == 'students')
          onClick=()=>setTab('students')
        ) Students
        Tab(
          active=(tab == 'teachers')
          onClick=()=>setTab('teachers')
        ) Teachers
        Tab(
          active=(tab == 'settings')
          onClick=()=>setTab('settings')
        )
          | Settings
          case ldap.status
            when 'connecting'
              span.pl-2.text-sm.text-grey-500 (connecting)
            when 'connected'
              span.pl-2.text-sm.text-green-500 (connected)
            when 'failure'
              span.pl-2.text-sm.text-red-500 (failure)

      case tab
        when 'students'
          Students
        when 'teachers'
          Teachers
        when 'settings'
          Settings
  `
}

render(
  pug`
    LdapConnectionProvider
      Main
  `,
  document.body,
  document.body.lastElementChild,
)